/*
 * Copyright (c) 2018-2023 Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

// Package apiservice_redlab_1208ls implements the RESTful API to the redlab
// 1208ls service
package apiservice_redlab_1208ls

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strconv"
	"strings"

	server "gitlab.com/perinet/generic/lib/httpserver"
	rbac "gitlab.com/perinet/generic/lib/httpserver/rbac"
	"gitlab.com/perinet/generic/lib/redlab/redlab_1208ls"
	webhelper "gitlab.com/perinet/generic/lib/utils/webhelper"
)

// Mesurement result data type
type Measurement struct {
	Value float64 `json:"value"`
	Unit  string  `json:"unit"`
}

// Channel abstraction type
type Redlab_1208ls_Data struct {
	Data Measurement `json:"data"`
	Gain float64     `json:"gain"`
	Mode string      `json:"mode"`
}

// Error type for unknown gain settings
type UnknownGainError struct{}

func (m *UnknownGainError) Error() string {
	return "Unknown gain"
}

var (
	logger log.Logger = *log.Default()

	cache [redlab_1208ls.Channel_Number]Redlab_1208ls_Data
)

func PathsGet() []server.PathInfo {
	return []server.PathInfo{
		{Url: "/redlab/1208ls/channel/0", Method: server.GET, Role: rbac.NONE, Call: Redlab_1208ls_channel_Get},
		{Url: "/redlab/1208ls/channel/1", Method: server.GET, Role: rbac.NONE, Call: Redlab_1208ls_channel_Get},
		{Url: "/redlab/1208ls/channel/2", Method: server.GET, Role: rbac.NONE, Call: Redlab_1208ls_channel_Get},
		{Url: "/redlab/1208ls/channel/3", Method: server.GET, Role: rbac.NONE, Call: Redlab_1208ls_channel_Get},
		{Url: "/redlab/1208ls/channel/4", Method: server.GET, Role: rbac.NONE, Call: Redlab_1208ls_channel_Get},
		{Url: "/redlab/1208ls/channel/5", Method: server.GET, Role: rbac.NONE, Call: Redlab_1208ls_channel_Get},
		{Url: "/redlab/1208ls/channel/6", Method: server.GET, Role: rbac.NONE, Call: Redlab_1208ls_channel_Get},
		{Url: "/redlab/1208ls/channel/7", Method: server.GET, Role: rbac.NONE, Call: Redlab_1208ls_channel_Get},

		{Url: "/redlab/1208ls/channel/0", Method: server.PATCH, Role: rbac.NONE, Call: Redlab_1208ls_channel_Patch},
		{Url: "/redlab/1208ls/channel/1", Method: server.PATCH, Role: rbac.NONE, Call: Redlab_1208ls_channel_Patch},
		{Url: "/redlab/1208ls/channel/2", Method: server.PATCH, Role: rbac.NONE, Call: Redlab_1208ls_channel_Patch},
		{Url: "/redlab/1208ls/channel/3", Method: server.PATCH, Role: rbac.NONE, Call: Redlab_1208ls_channel_Patch},
		{Url: "/redlab/1208ls/channel/4", Method: server.PATCH, Role: rbac.NONE, Call: Redlab_1208ls_channel_Patch},
		{Url: "/redlab/1208ls/channel/5", Method: server.PATCH, Role: rbac.NONE, Call: Redlab_1208ls_channel_Patch},
		{Url: "/redlab/1208ls/channel/6", Method: server.PATCH, Role: rbac.NONE, Call: Redlab_1208ls_channel_Patch},
		{Url: "/redlab/1208ls/channel/7", Method: server.PATCH, Role: rbac.NONE, Call: Redlab_1208ls_channel_Patch},
	}
}

// Handler for GET request to url "/redlab/1208ls/channel0-7"
func Redlab_1208ls_channel_Get(w http.ResponseWriter, r *http.Request) {
	http_status := http.StatusInternalServerError
	var res json.RawMessage
	channel, err := channel_from_url(r.URL.String())
	if err != nil {
		res = json.RawMessage(`{"error": "` + err.Error() + `"}`)
	} else {
		http_status, res = get_response(channel)
	}
	webhelper.JsonResponse(w, http_status, res)
}

// Handler for PATCH request to url "/redlab/1208ls/ch0"
func Redlab_1208ls_channel_Patch(w http.ResponseWriter, r *http.Request) {
	http_status := http.StatusInternalServerError
	var res json.RawMessage
	channel, err := channel_from_url(r.URL.String())
	if err != nil {
		res = json.RawMessage(`{"error": "` + err.Error() + `"}`)
	} else {
		http_status, res = patch_response(channel, r)
	}
	if len(res) > 0 {
		webhelper.JsonResponse(w, http_status, res)
	} else {
		webhelper.EmptyResponse(w, http_status)
	}

}

func channel_from_url(url string) (int, error) {
	tmp := strings.Split(url, "/")
	return strconv.Atoi(tmp[4])
}

func init() {
	logger.SetPrefix("apiservice-redlab: ")
	for index := range cache {
		cache[index].Gain = 20.0
		cache[index].Mode = "differential"
		cache[index].Data.Value = 0.0
		cache[index].Data.Unit = "V"
	}
	redlab_1208ls.Start()
}

func get_channel_setting(input Redlab_1208ls_Data) (redlab_1208ls.Gain, error) {
	ret := redlab_1208ls.Gain_Differential_20_0V
	var err error = nil

	if input.Mode == "single" {
		ret = redlab_1208ls.Gain_SingelEnded_10_0V
	} else {
		switch input.Gain {
		case 1.0:
			ret = redlab_1208ls.Gain_Differential_1_0V
		case 1.25:
			ret = redlab_1208ls.Gain_Differential_1_25V
		case 2.0:
			ret = redlab_1208ls.Gain_Differential_2_0V
		case 2.5:
			ret = redlab_1208ls.Gain_Differential_2_5V
		case 4.0:
			ret = redlab_1208ls.Gain_Differential_4_0V
		case 5.0:
			ret = redlab_1208ls.Gain_Differential_5_0V
		case 10.0:
			ret = redlab_1208ls.Gain_Differential_10_0V
		case 20.0:
			ret = redlab_1208ls.Gain_Differential_20_0V
		default:
			err = &UnknownGainError{}
		}
	}
	return ret, err
}

func get_response(index int) (int, json.RawMessage) {
	var http_status int = http.StatusOK
	var res json.RawMessage
	var channel redlab_1208ls.Channel = redlab_1208ls.Channel(index)
	var value float64
	var err error
	var gain redlab_1208ls.Gain
	gain, err = get_channel_setting(cache[index])
	if err == nil {
		value, err = redlab_1208ls.Analog_in(channel, gain)
	}
	if err != nil {
		http_status = http.StatusInternalServerError
		res = json.RawMessage(`{"error": "` + err.Error() + `"}`)
	} else {
		cache[index].Data.Value = value
		res, err = json.Marshal(cache[index])
		if err != nil {
			http_status = http.StatusInternalServerError
			res = json.RawMessage(`{"error": "` + err.Error() + `"}`)
		}
	}
	return http_status, res
}

func patch_response(index int, r *http.Request) (int, json.RawMessage) {
	var http_status int = http.StatusOK
	var res json.RawMessage
	payload, _ := io.ReadAll(r.Body)

	tmp := cache[index]
	err := json.Unmarshal(payload, &tmp)
	if err != nil {
		http_status = http.StatusBadRequest
		res = json.RawMessage(`{"error": "` + err.Error() + `"}`)
	} else {
		cache[index] = tmp
	}
	return http_status, res
}
